package com.kenfogel.tweetdemo02.presentation;

import com.kenfogel.tweetdemo02.business.TwitterInfo;
import com.kenfogel.tweetdemo02.business.TwitterInfoCell;
import com.kenfogel.tweetdemo02.business.TwitterTimelineTask;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This demo shows how you can display a timeline. It uses the ListView control
 * that can display a list of controls. In this demo it is a list of VBoxes that
 * can display such as images images and text. Buttons can be easily added.
 *
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 *
 * This demo shows a threaded example that searched twitter for a specific term.
 *
 * This demo does not use threads. Instead, you press a button to get the next
 * 20 tweets from your timeline.
 *
 * @author tomo
 * @author Ken Fogel
 */
public class TwitterListViewDemo extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TwitterListViewDemo.class);

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    private TwitterTimelineTask task;
    private Button showButton;
    private Button exitButton;
    private ListView<TwitterInfo> listView;

    /**
     * This builds the very basic UI
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {

        showButton = new Button("Show home timeline");
        exitButton = new Button("Exit");
        listView = new ListView<>();

        BorderPane root = new BorderPane();
        root.setPadding(new Insets(10));
        root.setTop(getHBoxButtons());
        root.setCenter(getHBoxView());
        Scene scene = new Scene(root, 600, 300);
        primaryStage.setTitle("Twitter ListView OF Timeline Demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Build an HBox for the buttons.
     *
     * @return The completed HBox
     */
    private Node getHBoxButtons() {
        HBox node = new HBox();
        node.setPadding(new Insets(10, 10, 10, 10));
        node.setSpacing(10);
        node.setAlignment(Pos.CENTER);
        node.getChildren().addAll(showButton, exitButton);
        showButton.setOnAction(this::handleShowTimeline);
        exitButton.setOnAction(this::handleStop);
        return node;
    }

    /**
     * Build an HBox for the ListView.
     *
     * @return The completed HBox
     */
    private Node getHBoxView() {
        HBox node = new HBox();
        node.setPadding(new Insets(0, 10, 10, 10));
        node.setSpacing(10);
        ObservableList<TwitterInfo> list = FXCollections.observableArrayList();
        listView.setItems(list);
        listView.setPrefWidth(600);
        listView.setCellFactory(p -> new TwitterInfoCell());
        node.getChildren().addAll(listView);

        // Event handler when a cell is selected
        listView.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends TwitterInfo> ov, TwitterInfo t, TwitterInfo t1) -> {
                    if (t != null) {
                        LOG.debug("Previous handle: " + t.getHandle());
                    }
                    if (t1 != null) {
                        LOG.debug("New handle: " + t1.getHandle());
                    }
                });
        return node;
    }

    /**
     * Event handler for the Show button
     *
     * @param e
     */
    private void handleShowTimeline(ActionEvent event) {
        //listView.getItems().clear();
        if (task == null) {
            task = new TwitterTimelineTask(listView.getItems());
            showButton.setText("Get next 20 tweets");
        }
        try {
            task.fillTimeLine(); 
        } catch (Exception ex) {
            LOG.error("Unable to display timeline", ex);
        }
    }

    /**
     * Event handler for the exit button
     *
     * @param e
     */
    private void handleStop(ActionEvent event) {
        Platform.exit();
    }
}
