package com.kenfogel.tweetdemo02.business;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ListCell for TwitterInfo This class represents the contents of an HBox that
 * contains tweet info
 *
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 *
 * @author tomo
 * @author Ken Fogel
 */
public class TwitterInfoCell extends ListCell<TwitterInfo> {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfoCell.class);

    /**
     * This method is called when ever cells need to be updated
     *
     * @param item
     * @param empty
     */
    @Override
    protected void updateItem(TwitterInfo item, boolean empty) {
        super.updateItem(item, empty);

        LOG.debug("updateItem");

        if (empty || item == null) {
            setText(null);
            setGraphic(null);
        } else {
            setGraphic(getTwitterInfoCell(item));
        }
    }

    /**
     * This method determines what the cell will look like. Here is where you
     * can add buttons or any additional information
     *
     * @param info
     * @return The node to be placed into the ListView
     */
    private Node getTwitterInfoCell(TwitterInfo info) {
        LOG.debug("getTwitterInfoCell");
        HBox node = new HBox();
        node.setSpacing(10);
        
        Image image = new Image(info.getImageURL(), 48, 48, true, false);
        ImageView imageView = new ImageView(image);
        
        Text name = new Text(info.getName());
        name.setWrappingWidth(450);
        
        Text text = new Text(info.getText());
        text.setWrappingWidth(450);
        
        Text count = new Text(""+info.getTweetID());
        LOG.debug("ID = : "+ info.getTweetID());
        count.setWrappingWidth(450);

        VBox vbox = new VBox();
        vbox.getChildren().addAll(name, text, count);
        node.getChildren().addAll(imageView, vbox);
        
        return node;
    }
}
