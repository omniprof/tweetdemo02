package com.kenfogel.tweetdemo02.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 * Twitter information for ListCell
 * This class defines which members of the Status object you wish to display.
 * 
 * This is based on the following
 * https://github.com/tomoTaka01/TwitterListViewSample.git
 * 
 * @author tomo
 */
public class TwitterInfo {
    
    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TwitterInfo.class);
    
    private final Status status;

    public TwitterInfo(Status status) {
        this.status = status;
    }

    public String getName() {
        return status.getUser().getName();
    }

    public String getText(){
        return status.getText();
    }

    public String getImageURL(){
        return status.getUser().getProfileImageURL();
    }
    
    public String getHandle() {
      return status.getUser().getScreenName();
    }
    
    public long getTweetID() {
        return status.getId();
    }
}
