TwitterDemo02

This program is based on the work found at https://github.com/tomoTaka01/TwitterListViewSample, the blog found at https://www.baeldung.com/twitter4j, and Twitter4J from Yusuke Yamamoto.

To run this sample you need to perform two steps. 

First, you need a developer account from Twitter https://developer.twitter.com/. With this account you will be able to use keys rather than a user name and password. These keys must be stored in a file named twitter4j.properties in src/main/resources. An empty file with just the property keys is included.

Second, you must retrieve the project:
https://gitlab.com/omniprof/desktop_projects_dependencies.git
This is a parent pom.xml that must be installed with the Maven goal install. You must do this before the first time you compile the project. 

The example from tomo taka used threads to display a filtered (searched) stream. I have removed all threading in this example and now it will display the timeline 20 tweets at a time. Pressing the button to show more tweets adds them to the end of the list. 

This code is meant as a jumping off point for my students who are working on a Twitter client in Java with JavaFX. It is the responsibility of my students to enhance and polish this as part of their project work.
